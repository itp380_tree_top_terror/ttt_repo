A jungle themed platformer with grappling capabilities. 
Has both first (for grappling) and third (for locomotion) person camera. 
Enemies shooting projectiles who can be killed via the grapple. 
Obstacles based on platform design. 
Third person camera will be rotatable and remain fixed relative to player character's orientation. 
Player can also run, jump - more abilities potentially after further design.