// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "JumpPad.generated.h"

UCLASS()
class TREE_TOP_TERROR_API AJumpPad : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AJumpPad();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

  UPROPERTY(VisibleAnywhere)
      class UBoxComponent* BoxComp;

  UFUNCTION()
      void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

  UFUNCTION()
      void Launch(ATree_Top_TerrorCharacter* Player);

  UAudioComponent* PlayJumpSound(USoundCue* Sound);

protected:
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	class USoundCue* LaunchCue;

	UPROPERTY(Transient)
	class UAudioComponent* JumpPadAC;
private:
    ATree_Top_TerrorCharacter* mPlayer = nullptr;
    
    UPROPERTY(EditDefaultsOnly)
    float UpSpeed = 1000;
    
};
