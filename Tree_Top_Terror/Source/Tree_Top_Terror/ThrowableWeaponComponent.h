// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "ThrowableWeaponComponent.generated.h"

/*
    When UThrowableWeaponComponent is added to an Actor, then the Actor can be thrown.
 */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TREE_TOP_TERROR_API UThrowableWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UThrowableWeaponComponent();
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

private:
    
    // if set to true then will throw the weapon
    bool canThrow = false;
    
public:
    
    // Throws the actor to which the component is attached
    virtual void Throw(FVector direction, float distance);
};
