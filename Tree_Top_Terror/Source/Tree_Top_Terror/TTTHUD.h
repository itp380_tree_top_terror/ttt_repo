// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "TTTHUD.generated.h"

/**
 * 
 */

UCLASS()
class TREE_TOP_TERROR_API UTTTHUD : public UUserWidget
{
	GENERATED_BODY()
	
public:
    
    // Sets the instructions
    void SetInstruction(FString);
    
    // Toggle the visibility of the instruction text
    void ShowInstruction(bool);
    
    // Toggles the crosshair
    void DisplayCrossHair(bool);
    
    // Display game over
    void DisplayGameOver();
    
protected:
    
    // The instruction to display to the user
    // this instruction string is bound to the UI
    // hence changing this field will change the UI
	UPROPERTY(Category = UserInterface, EditAnywhere, BlueprintReadWrite)
    FString instruction;

    // whether to show the instruction text box or not
    UPROPERTY(Category = UserInterface, EditAnywhere, BlueprintReadWrite)
    ESlateVisibility show;
    
    // whether to show the crosshair
    UPROPERTY(Category = UserInterface, EditAnywhere, BlueprintReadWrite)
    ESlateVisibility displayCrossHair = ESlateVisibility::Hidden;
    
    // whether to show the game over text
    UPROPERTY(Category = UserInterface, EditAnywhere, BlueprintReadWrite)
    ESlateVisibility displayGameOver = ESlateVisibility::Hidden;

};
