// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Tree_Top_Terror.h"
#include "Tree_Top_TerrorGameMode.h"
#include "Tree_Top_TerrorCharacter.h"
#include "Runtime/Engine/Classes/GameFramework/HUD.h"
//#include "Runtime/UMG/Public/Blueprint/UserWidget.h"

ATree_Top_TerrorGameMode::ATree_Top_TerrorGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
    
    // default HUD class
    static ConstructorHelpers::FClassFinder<AHUD> HUD(TEXT("/Game/Blueprints/HUD"));
    if (HUD.Class != NULL) {
        HUDClass = HUD.Class;
    }
}

void ATree_Top_TerrorGameMode::FinishLevel()
{
    UGameplayStatics::OpenLevel(this, "MainLevel");
}
