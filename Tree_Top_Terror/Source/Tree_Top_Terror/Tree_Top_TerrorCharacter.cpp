// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Tree_Top_Terror.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "Tree_Top_TerrorCharacter.h"
#include "Level1ScriptActor.h"
#include "Tree_Top_TerrorGameMode.h"
#include "Sound/SoundCue.h"

//////////////////////////////////////////////////////////////////////////
// ATree_Top_TerrorCharacter

ATree_Top_TerrorCharacter::ATree_Top_TerrorCharacter()
	: mRightClickDown(false)
	, IsAiming(false)
	, mSavedCameraDistance(330.0f)
	, mDesiredCameraDistance(430.0f)
	, mScrollWheelFactor(20.0f)
	, mDesiredYOffset(0.0f)
	, mDesiredZOffset(80.0f)
	, mCanFire(true)
	, GrappleRange(1500.0f)
	, mGrappling(false)
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = mSavedCameraDistance; // The camera follows at this distance behind the character	
	CameraBoom->SocketOffset.Z = mDesiredZOffset;
	CameraBoom->SocketOffset.Y = mDesiredYOffset;
	CameraBoom->bUsePawnControlRotation = false; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm



	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	PrimaryActorTick.bCanEverTick = true;
}

//////////////////////////////////////////////////////////////////////////
// Input

void ATree_Top_TerrorCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("RightClick", IE_Pressed, this, &ATree_Top_TerrorCharacter::OnRightClickDown);
	PlayerInputComponent->BindAction("RightClick", IE_Released, this, &ATree_Top_TerrorCharacter::OnRightClickUp);
	PlayerInputComponent->BindAction("LeftClick", IE_Pressed, this, &ATree_Top_TerrorCharacter::OnLeftClickDown);
	PlayerInputComponent->BindAction("LeftClick", IE_Released, this, &ATree_Top_TerrorCharacter::OnLeftClickUp);

	PlayerInputComponent->BindAction("ScrollMouseDown", IE_Pressed, this, &ATree_Top_TerrorCharacter::OnMouseWheelDown);
	PlayerInputComponent->BindAction("ScrollMouseUp", IE_Released, this, &ATree_Top_TerrorCharacter::OnMouseWheelUp);

	PlayerInputComponent->BindAction("ToggleCamera", IE_Pressed, this, &ATree_Top_TerrorCharacter::OnToggleCamera);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATree_Top_TerrorCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATree_Top_TerrorCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &ATree_Top_TerrorCharacter::TurnHandler);//::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ATree_Top_TerrorCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ATree_Top_TerrorCharacter::LookUpAtRate);
    
    // Rotation
    PlayerInputComponent->BindAxis("Rotate", this, &ATree_Top_TerrorCharacter::Rotate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ATree_Top_TerrorCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ATree_Top_TerrorCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ATree_Top_TerrorCharacter::OnResetVR);
}

void ATree_Top_TerrorCharacter::BeginPlay()
{
	Super::BeginPlay();

	UWorld* World = GetWorld();
	if (World)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = Instigator;
		// Need to set rotation like this because otherwise gun points down 
		FRotator Rotation(0.0f, 0.0f, -90.0f);

		// Spawn the Weapon 
		MyWeapon = World->SpawnActor<AGrappleGun>(WeaponClass, FVector::ZeroVector, Rotation, SpawnParams);

		if (MyWeapon)
		{
			// This is attached to "WeaponPoint" which is defined in the skeleton 
			MyWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("GrappleGunSocket"));
			MyWeapon->SetMyOwner(this);

			MyWeapon->SetActorHiddenInGame(true);
		}


		
		/*FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = Instigator;*/

		Rotation = { 0.0f, 0.0f, 0.0f };

		MyGrapple = World->SpawnActor<AGrapple>(GrappleClass, FVector::ZeroVector, Rotation, SpawnParams);

		if (MyGrapple && MyWeapon)
		{
			MyWeapon->AttachGrapple(MyGrapple);
		}
	}
}

void ATree_Top_TerrorCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime); // Call parent class tick function
	if (!FMath::IsNearlyEqual(CameraBoom->TargetArmLength, mDesiredCameraDistance))
	{
		CameraBoom->TargetArmLength = FMath::FInterpTo(CameraBoom->TargetArmLength, mDesiredCameraDistance, DeltaTime, 5);
	}

	if (!FMath::IsNearlyEqual(CameraBoom->SocketOffset.Y, mDesiredYOffset))
	{
		CameraBoom->SocketOffset.Y = FMath::FInterpTo(CameraBoom->SocketOffset.Y, mDesiredYOffset, DeltaTime, 2);
	}

	if (!FMath::IsNearlyEqual(CameraBoom->SocketOffset.Z, mDesiredYOffset))
	{
		CameraBoom->SocketOffset.Z = FMath::FInterpTo(CameraBoom->SocketOffset.Z, mDesiredZOffset, DeltaTime, 2);
	}

}

void ATree_Top_TerrorCharacter::Rotate(float angle)
{
    auto rotator = GetActorRotation();
    auto newRotator = FRotator(0, angle * GetWorld()->GetDeltaSeconds() * this->angle, 0);
    rotator += newRotator;
    SetActorRotation(rotator);
}

void ATree_Top_TerrorCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ATree_Top_TerrorCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
		Jump();
	}
}

void ATree_Top_TerrorCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
		StopJumping();
	}
}

void ATree_Top_TerrorCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATree_Top_TerrorCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ATree_Top_TerrorCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		//const FRotator Rotation = Controller->GetControlRotation();
        //const FRotator YawRotation(0, Rotation.Yaw, 0);

        // Move in the direction where the player is looking
        const FVector Direction = GetActorForwardVector();
        
		// get forward vector
		//const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		// if aiming, move slower
		if (IsAiming)
		{
			Value /= 3.0f;
		}

		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ATree_Top_TerrorCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator rot = GetActorRotation();
		//const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, rot.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		//const FVector dir = FVector(-GetActorForwardVector().Y, GetActorForwardVector().X, 0);
		
		// if aiming, move slower
		if (IsAiming)
		{
			Value /= 3.0f;
		}

		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ATree_Top_TerrorCharacter::FireGrapple()
{

	mCanFire = false;

	// show the grapple
	MyGrapple->SetActorHiddenInGame(false);

	PlayCharacterSound(FireGrapplesCue);

	// fire action here
	FVector target = GrappleTrace();

	FName socketName("GrappleGunSocket");
	FVector socketLoc;
	FRotator socketRot;

	GetMesh()->GetSocketWorldLocationAndRotation(socketName, socketLoc, socketRot);

	socketLoc += GetActorForwardVector() * 50;

	MyGrapple->SetActorLocation(socketLoc);
	MyGrapple->SetActorRotation(FollowCamera->GetForwardVector().Rotation() + FRotator(270,0,0));

	MyGrapple->SetTarget(GrappleTrace());
	MyGrapple->Fired();

	// restrict firing
	//GetWorldTimerManager().SetTimer(mGrappleTimer, this, &ATree_Top_TerrorCharacter::EnableFire, 2.0f, false);
}

void ATree_Top_TerrorCharacter::EnableFire()
{
	mCanFire = true;
}

void ATree_Top_TerrorCharacter::OnRightClickDown()
{
	mRightClickDown = true;
}

void ATree_Top_TerrorCharacter::OnRightClickUp()
{
	mRightClickDown = false;
}

void ATree_Top_TerrorCharacter::TurnHandler(float Value)
{
	AddControllerYawInput(Value);

	// if we're not aiming
	if (!IsAiming)
	{
		// if we're holding the right mouse button down
		if (mRightClickDown)
		{
			// rotate character by value
			bUseControllerRotationYaw = true;

			// if our left mouse button is also down
			if (mLeftClickDown)
			{
				// move forward
				AddMovementInput(GetActorForwardVector());
			}
		}
		else
		{
			bUseControllerRotationYaw = false;
		}
	}
	else //is aiming
	{
		// always follow where the camera is pointing
		bUseControllerRotationYaw = true;
	}
}

FVector ATree_Top_TerrorCharacter::GrappleTrace()
{

	static FName WeaponFireTag = FName(TEXT("GrappleTrace"));
	// static FName MuzzleSocket = FName(TEXT("MuzzleFlashSocket"));

	// Start from the muzzle's position 
	FVector StartPos = FollowCamera->GetComponentLocation();//->GetLocation(); //WeaponMesh->GetSocketLocation(MuzzleSocket);
	// Get forward vector of MyPawn 
	FVector Forward = FollowCamera->GetForwardVector();//MyOwner->GetActorForwardVector();//.Normalize();

													   // Calculate end position 
	FVector EndPos = Forward * GrappleRange + StartPos;

	// Perform trace to retrieve hit info 
	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	// This fires the ray and checks against all objects w/ collision 
	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByObjectType(Hit, StartPos, EndPos, FCollisionObjectQueryParams::AllObjects, TraceParams);

	// Did this hit anything? 
	if (Hit.bBlockingHit)
	{
		// TODO: Actually do something 
		return Hit.ImpactPoint;
	}
	return EndPos;
}

void ATree_Top_TerrorCharacter::OnLeftClickDown()
{
	mLeftClickDown = true;
	if (IsAiming && mCanFire)
	{
		FireGrapple();
	}
}

void ATree_Top_TerrorCharacter::OnLeftClickUp()
{
	mLeftClickDown = false;
}

void ATree_Top_TerrorCharacter::OnMouseWheelDown()
{
	if (mDesiredCameraDistance < 1000.0f && !IsAiming)
	{
		mDesiredCameraDistance += mScrollWheelFactor;
	}
}

void ATree_Top_TerrorCharacter::OnMouseWheelUp()
{
	if (mDesiredCameraDistance > 70.0f && !IsAiming)
	{
		mDesiredCameraDistance -= mScrollWheelFactor;
	}
}

void ATree_Top_TerrorCharacter::OnToggleCamera()
{
	// briefly restrict firing until camera is positioned appropriately
	mCanFire = false;
	// was aiming
	if (IsAiming)
	{
		PlayCharacterSound(StowGrappleCue);
		// despawn gun here
		if (MyWeapon)
		{
			MyWeapon->SetActorHiddenInGame(true);
		}

		// clear the grapple timer so that we don't reenable
		GetWorldTimerManager().ClearTimer(mGrappleTimer);

		// now is not aiming
		mDesiredCameraDistance = mSavedCameraDistance;
		mDesiredYOffset = 0.0f;
		mDesiredZOffset = 80.0f;
	}
	else // was not aiming
	{
		PlayCharacterSound(AimCue);
		// spawn gun here
		if (MyWeapon)
		{
			MyWeapon->SetActorHiddenInGame(false);
		}

		// now is aiming
		mSavedCameraDistance = CameraBoom->TargetArmLength;
		mDesiredCameraDistance = 70;
		mDesiredYOffset = 50.0f;
		mDesiredZOffset = 40.0f;
		// if our grapple wasn't fired before switching out then into aiming mode
		if (!MyGrapple->GrappleIsOut())
		{
			GetWorldTimerManager().SetTimer(mGrappleTimer, this, &ATree_Top_TerrorCharacter::EnableFire, .5f, false);
		}
	}

	// aiming state appropriately toggled
	IsAiming = !IsAiming;
    
    // Tell the level to update the status of the cross hair
    ALevel1ScriptActor *level = Cast<ALevel1ScriptActor>(GetLevel()->GetLevelScriptActor());
    if (level) {
        level->DisplayCrossHair(IsAiming);
    }
}

void ATree_Top_TerrorCharacter::ProcessDamage(float damage)
{
	PlayCharacterSound(TakeDMGCue);
    Health -= damage;
    // Player is dead
    if (Health <= 0 && !isDead) {
        isDead = true;
        
        // Tell the level to update the status of the game
        ALevel1ScriptActor *level = Cast<ALevel1ScriptActor>(GetLevel()->GetLevelScriptActor());
        if (level) {
            level->DisplayGameOver();
        }
        
        // go back to main menu
        FTimerHandle handle;
        GetWorld()->GetTimerManager().SetTimer(handle, this, &ATree_Top_TerrorCharacter::DeathCallback, 3, false);
    }
}

void ATree_Top_TerrorCharacter::DeathCallback()
{
    if (AGameMode* game = UGameplayStatics::GetGameMode(this)) {
        auto mode = Cast<ATree_Top_TerrorGameMode>(game);
        mode->FinishLevel();
    }
}

UAudioComponent* ATree_Top_TerrorCharacter::PlayCharacterSound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
	}
	return AC;
}
