// Fill out your copyright notice in the Description page of Project Settings.

#include "Tree_Top_Terror.h"
#include "GrappleGun.h"


// Sets default values
AGrappleGun::AGrappleGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	RootComponent = WeaponMesh;

	


}

// Called when the game starts or when spawned
void AGrappleGun::BeginPlay()
{
	Super::BeginPlay();
	
	
}

void AGrappleGun::SetMyOwner(class ATree_Top_TerrorCharacter* owner) 
{
	MyOwner = owner;
}

// Called every frame
void AGrappleGun::Tick( float DeltaTime )
{
	Super::Tick(DeltaTime);

}

void AGrappleGun::AttachGrapple(class AGrapple* grapple)
{
	MyGrapple = grapple;

	FName socketName("GrappleGunMuzzle");
	FVector socketLoc;
	FRotator socketRot;
	
	WeaponMesh->GetSocketWorldLocationAndRotation(socketName, socketLoc, socketRot);

	// FVector socketLoc = WeaponMesh->GetSocketLocation("GrappleGunMuzzle");

	MyGrapple->SetActorLocation(socketLoc);
	MyGrapple->SetActorRotation(socketRot);
	//MyGrapple->AttachToComponent(WeaponMesh, FAttachmentTransformRules::KeepRelativeTransform, TEXT("GrappleGunMuzzle"));

	MyGrapple->SetActorHiddenInGame(true);

	MyGrapple->SetGun(this);

	MyGrapple->SetMyOwner(MyOwner);
}

