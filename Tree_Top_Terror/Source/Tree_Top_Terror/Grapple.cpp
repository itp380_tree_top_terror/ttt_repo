// Fill out your copyright notice in the Description page of Project Settings.


#include "Tree_Top_Terror.h"
#include "Grapple.h"
#include "GrappleGun.h"
#include "Enemy.h"
#include "Coin.h"
#include "Tree_Top_TerrorCharacter.h"
#include "Sound/SoundCue.h"
// #include "TriggerVolume.h"




// Sets default values
AGrapple::AGrapple()
	: mTarget(FVector::ZeroVector)
	, mArrived(false)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	
	GrappleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GrappleMesh"));
	RootComponent = GrappleMesh;
	
}

// Called when the game starts or when spawned
void AGrapple::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGrapple::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	//if (!mTarget.IsNearlyZero(.01))
	if (mTarget != FVector::ZeroVector)
	{
		// if our grapple hasn't yet reached the target
		if (!mArrived)
		{
			FVector Direction = mTarget - GetActorLocation();
			Direction.Normalize();

			// add movement in that direction
			AddActorWorldOffset(Direction * mGrappleSpeed * DeltaTime); // , true);
			FVector testVec = mTarget - GetActorLocation();
			if ((mTarget - GetActorLocation()).IsNearlyZero(8))
			{
				GrappleArrived();
			}

			TArray<AActor*> collided;
			// process collision here
			GetOverlappingActors(collided);//, AEnemy::StaticClass);

			for (auto& act : collided) // if 'collided' is underscored in a red squigly (ie it's showing up as an error) this is a mistake of the compiler
			{						  // just go ahead and compile, it works
				if (!act->IsA(ATree_Top_TerrorCharacter::StaticClass()) && !act->IsA(AGrappleGun::StaticClass()))
					// && !act->IsA(ATriggerVolume::StaticClass()))
				{
					GrappleArrived();
				}
									  // if the collision is with an enemy
				if (act->IsA(AEnemy::StaticClass()))
				{
					// do damage to it
					PlayStrikeSound(EnemyStruckCue);
					static_cast<AEnemy*>(act)->ProcessDamage(100);
					MyOwner->AcquireCoin(20);
				}
				else if (act->IsA(ACoin::StaticClass()))
				{
					mCoin = static_cast<ACoin*>(act);
					mCoin->AttachToComponent(GrappleMesh, FAttachmentTransformRules::SnapToTargetIncludingScale);
				}
			}
		}
		else // return to the character
		{
			FName socketName("GrappleGunSocket");
			FVector socketLoc;
			FRotator socketRot;
			MyOwner->GetMesh()->GetSocketWorldLocationAndRotation(socketName, socketLoc, socketRot);

			socketLoc += MyOwner->GetActorForwardVector() * 50;

			FVector Direction = socketLoc - GetActorLocation();
			Direction.Normalize();

			// add movement in that direction
			AddActorWorldOffset(Direction * mGrappleSpeed * DeltaTime); // , true);

			if ((socketLoc - GetActorLocation()).IsNearlyZero(15))
			{
				EndGrapple();
			}
		}
	}
}

void AGrapple::GrappleArrived()
{
	mArrived = true;
	GetWorldTimerManager().ClearTimer(mArriveTimer);
	GetWorldTimerManager().SetTimer(mTerminateTimer, this, &AGrapple::EndGrapple, .85f, false);
}

void AGrapple::EndGrapple()
{
	if (mCoin != nullptr)
	{
		mCoin->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		//mCoin->Destroy();
		mCoin = nullptr;
		//MyOwner->AcquireCoin(10);
	}
	mTarget = FVector::ZeroVector;
	mArrived = false;
	SetActorHiddenInGame(true);
	PlayStrikeSound(GrappleReturnCue);
	GetWorldTimerManager().ClearTimer(mTerminateTimer);
	MyOwner->EnableFire();
}

void AGrapple::Fired()
{
	GetWorldTimerManager().SetTimer(mArriveTimer, this, &AGrapple::GrappleArrived, .85f, false);
}

UAudioComponent* AGrapple::PlayStrikeSound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
	}
	return AC;
}