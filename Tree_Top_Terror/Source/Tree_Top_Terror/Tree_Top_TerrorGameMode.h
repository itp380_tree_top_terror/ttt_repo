// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "Tree_Top_TerrorGameMode.generated.h"

UCLASS(minimalapi)
class ATree_Top_TerrorGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATree_Top_TerrorGameMode();
    
    UFUNCTION(BlueprintCallable, category = LevelManagement)
    void FinishLevel();
};



