// Fill out your copyright notice in the Description page of Project Settings.

#include "Tree_Top_Terror.h"
#include "Level2ScriptActor.h"
#include "TTTHUD.h"

void ALevel2ScriptActor::BeginPlay()
{
    Super::BeginPlay();
    if (this->HUD) {
        this->HUD->ShowInstruction(false);
    }
}
