// Fill out your copyright notice in the Description page of Project Settings.

#include "Tree_Top_Terror.h"
#include "EnemyAIController.h"
#include "Enemy.h"
#include "EnemyAnimInstance.h"

void AEnemyAIController::BeginPlay()
{
    Super::BeginPlay();
    
    // Main player
    this->Player = UGameplayStatics::GetPlayerCharacter(this, 0);
    
    // Get the animation instance to manage the animations
    if (ACharacter* character = GetCharacter()) {
        if (USkeletalMeshComponent* mesh = character->GetMesh()) {
            this->AnimInstance = Cast<UEnemyAnimInstance>(mesh->GetAnimInstance());
        }
    }
    
}

void AEnemyAIController::Tick(float deltaTime)
{
    Super::Tick(deltaTime);
    
    // The enemy managed by this controller
    auto enemyCharacter = Cast<AEnemy>(GetCharacter());
    
    // Edge case. Should not happen
    if (Player == nullptr || enemyCharacter == nullptr) {
        return;
    }
    
    switch (currentState) {
        case Idle:
            // Set the timer
            if (UWorld* world = GetWorld()) {
                if (FTimerManager* manager = &world->GetTimerManager()) {
                    if (!manager->TimerExists(idleStateTimerHandle)) {
                        manager->SetTimer(idleStateTimerHandle, this, &AEnemyAIController::IdleStateTimerCallback, IdleStateTime, false);
                    }
                }
            }
            break;
        case Find:
            
            // This will trigger the find animation
            AnimInstance->FindPlayer = true;
            
            // If the player is within the enemy range
            if (PlayerWithInRange(Player->GetActorLocation())) {
                // Rotate the enemy to face the player
                RotateEnemy(Player->GetActorLocation());
                currentState = Throw;
                AnimInstance->FindPlayer = false;
            }
            break;
        case Throw:
            // This will trigger the throw animation
            AnimInstance->ThrowWeapon = true;

            // Spawn the weapon
            enemyCharacter->SpawnWeapon();
            break;
        case Dying:
            // Play dead animation
            AnimInstance->Dead = true;
            
            currentState = Dead;
            break;
        case Dead:
        default:
            break;
    }
}

/******************* CALLBACKS ***********************/

void AEnemyAIController::ThrowWeapon()
{
    auto enemyCharacter = Cast<AEnemy>(GetCharacter());
    if (enemyCharacter->IsEnemyDead()) {
        return;
    }
    
    // Throw the object at the player
    enemyCharacter->Throw(Player->GetActorLocation());
    
    currentState = Idle;
    AnimInstance->ThrowWeapon = false;
}

void AEnemyAIController::IdleStateTimerCallback()
{
    // if the enemy is dead, then don't change any state
    auto enemyCharacter = Cast<AEnemy>(GetCharacter());
    if (enemyCharacter->IsEnemyDead()) {
        return;
    }
    
    // if the player is within bounds then just rotate and throw
    if (PlayerWithInRange(Player->GetActorLocation())) {
        // Rotate the enemy to face the player
        RotateEnemy(Player->GetActorLocation());
        currentState = Throw;
    } else {
        currentState = Find;
    }

    GetWorld()->GetTimerManager().ClearTimer(idleStateTimerHandle);
    idleStateTimerHandle = FTimerHandle();
}

void AEnemyAIController::ManageTransitionFromThrowToIdle()
{
//    currentState = Idle;
//    AnimInstance->ThrowWeapon = false;
}

/******************* Helper ********************/

void AEnemyAIController::EnemyDead()
{
    currentState = Dying;
}

bool AEnemyAIController::PlayerWithInRange(FVector playerLocation)
{
    // the enemy controlled by the controller
    auto enemyCharacter = Cast<AEnemy>(GetCharacter());
    
    if (enemyCharacter) {
        // Enemy rrange
        float maxRange = enemyCharacter->GetMaxRange();
        // Distance between the player and the enemy
        float distance = FVector::Dist(playerLocation, enemyCharacter->GetActorLocation());
        if (distance <= maxRange) {
            return true;
        }
    }
    
    return false;
}

void AEnemyAIController::RotateEnemy(FVector playerLocation)
{
    auto enemyCharacter = Cast<AEnemy>(GetCharacter());
    if (enemyCharacter) {
        // Vector from the enemy to the player
        auto vec = playerLocation - enemyCharacter->GetActorLocation();
        vec.Z = 0;
        
        // Rotate the enemy to look at the player
        auto rotator = vec.Rotation();
        enemyCharacter->SetActorRotation(rotator);
    }
}

float AEnemyAIController::DistanceFromPlayer(FVector playerLocation)
{
    auto enemyCharacter = Cast<AEnemy>(GetCharacter());
    if (enemyCharacter) {
        return FVector::Dist(playerLocation, enemyCharacter->GetActorLocation());
    }
    return -1;
}


