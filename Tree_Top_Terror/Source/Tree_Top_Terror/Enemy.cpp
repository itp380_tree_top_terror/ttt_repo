// Fill out your copyright notice in the Description page of Project Settings.

#include "Tree_Top_Terror.h"
#include "Enemy.h"
#include "EnemyAIController.h"
#include "RangedWeapon.h"
#include "Runtime/Engine/Classes/Animation/AnimInstance.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    // Controller class for the enemy
    // only works if this enemy is placed in the level
    AIControllerClass = AEnemyAIController::StaticClass();
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEnemy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AEnemy::SpawnWeapon()
{
    if (spawnedWeapon == nullptr)
    {
        if (RangedWeaponClass)
        {
            if (UWorld* World = GetWorld())
            {
                FActorSpawnParameters SpawnParams;
                SpawnParams.Owner = this;
                SpawnParams.Instigator = Instigator;
                // Spawn the Weapon
                spawnedWeapon = World->SpawnActor<ARangedWeapon>(RangedWeaponClass, FVector::ZeroVector,
                                                                 FRotator::ZeroRotator, SpawnParams);
                if (spawnedWeapon)
                {
                    // This is attached to "WeaponPoint" which is defined in the skeleton
                    spawnedWeapon->AttachToComponent(GetMesh(),
                                                FAttachmentTransformRules::SnapToTargetIncludingScale,
                                                TEXT("RightHand"));
                }
            }
        }
    }
}

void AEnemy::Throw(FVector playerLocation)
{
    if (spawnedWeapon != nullptr) {
        spawnedWeapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
        spawnedWeapon->Throw(playerLocation);
        spawnedWeapon = nullptr;
    }
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

void AEnemy::ProcessDamage(float damage)
{
    Health -= damage;
    if (Health <= 0) {
        if(AEnemyAIController* controller = Cast<AEnemyAIController>(GetController())) {
            controller->EnemyDead();
			// GetWorldTimerManager().SetTimer(mGrappleTimer, this, &ATree_Top_TerrorCharacter::EnableFire, .5f, false);
			FTimerHandle handle;
			GetWorldTimerManager().SetTimer(handle, this, &AEnemy::DieAlready, 5.0f, false);
        }
    }
}

