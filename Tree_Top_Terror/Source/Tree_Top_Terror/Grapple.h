// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Grapple.generated.h"

UCLASS()
class TREE_TOP_TERROR_API AGrapple : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGrapple();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	void SetGun(class AGrappleGun* gun) { mTarGun = gun; }
	void SetTarget(FVector tar) { mTarget = tar; }
	void ClearTarget() { mTarget = FVector::ZeroVector; }

	void SetMyOwner(class ATree_Top_TerrorCharacter* owner) { MyOwner = owner; }

	bool mArrived;

	UAudioComponent* PlayStrikeSound(USoundCue* Sound);

	void GrappleArrived();
	void EndGrapple();
	void Fired();
	bool GrappleIsOut() { return mTarget != FVector::ZeroVector; }

	FTimerHandle mArriveTimer;
	FTimerHandle mTerminateTimer;

protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	UStaticMeshComponent* GrappleMesh;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	class USoundCue* EnemyStruckCue;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	class USoundCue* GrappleReturnCue;

	UPROPERTY(Transient)
	class UAudioComponent* GrappleAC;
	
private:
	class AGrappleGun* mTarGun;
	FVector mTarget;
	class ATree_Top_TerrorCharacter* MyOwner;
	double mGrappleSpeed = 2000;
	class ACoin* mCoin;
};
