// Fill out your copyright notice in the Description page of Project Settings.

#include "Tree_Top_Terror.h"
#include "RangedWeapon.h"
#include "Tree_Top_TerrorCharacter.h"
#include "Enemy.h"

// Sets default values
ARangedWeapon::ARangedWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    // Create the static mesh component
    staticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RootComponent"));
    RootComponent = staticMeshComponent;
}

// Called when the game starts or when spawned
void ARangedWeapon::BeginPlay()
{
	Super::BeginPlay();
    
    UCapsuleComponent* collision = FindComponentByClass<UCapsuleComponent>();
    if (collision) {
        collision->OnComponentBeginOverlap.AddDynamic(this, &ARangedWeapon::OnOverlap);
    }
    
    // 7 sec timer to destroy itself
    FTimerHandle handle;
    GetWorld()->GetTimerManager().SetTimer(handle, this, &ARangedWeapon::DestroyTimer, lifeTime, false);
}

void ARangedWeapon::OnOverlap(class UPrimitiveComponent* Comp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool FromSweep, const FHitResult& SweepResult)
{
    AEnemy* spawner = Cast<AEnemy>(OtherActor);
    if (spawner == nullptr) {
        Destroy();
    }
    
    // if we collided with the player
    if (ATree_Top_TerrorCharacter* player = Cast<ATree_Top_TerrorCharacter>(OtherActor)) {
        player->ProcessDamage(damage);
    }
}

// Called every frame
void ARangedWeapon::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    
    auto currentLocation = GetActorLocation();
    auto newLocation = currentLocation + (DeltaTime * speed * direction);
    SetActorLocation(newLocation);
}

// Callback
void ARangedWeapon::DestroyTimer()
{
    Destroy();
}

void ARangedWeapon::Throw(FVector enemyLocation)
{
    auto currentLocation = GetActorLocation();
    this->direction = enemyLocation - currentLocation;
    this->direction.Normalize();
}

