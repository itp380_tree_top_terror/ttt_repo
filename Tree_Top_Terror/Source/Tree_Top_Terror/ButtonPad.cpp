// Fill out your copyright notice in the Description page of Project Settings.

#include "Tree_Top_Terror.h"
#include "Tree_Top_TerrorCharacter.h"
#include "ButtonPad.h"


// Sets default values
AButtonPad::AButtonPad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

  BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
  BoxComp->SetWorldScale3D(FVector(200, 200, 10));
  BoxComp->SetupAttachment(RootComponent);
  BoxComp->OnComponentBeginOverlap.AddDynamic(this, &AButtonPad::OnOverlapBegin);
}

// Called when the game starts or when spawned
void AButtonPad::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AButtonPad::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AButtonPad::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    if (OtherActor && (OtherActor != this) && OtherComp)
    {
        auto Player = Cast<ATree_Top_TerrorCharacter>(OtherActor);
        if (Player)
        {
            MoveBlock();
        }
    }
}

void AButtonPad::MoveBlock()
{
    mButtonSellected = !mButtonSellected;
    if (mFloatingBlock)
    {
        //mFloatingBlock->MoveBlock();
    }
}
