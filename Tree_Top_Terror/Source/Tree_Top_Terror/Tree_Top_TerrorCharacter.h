// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "GrappleGun.h"
#include "Grapple.h"
#include <string>
#include "GameFramework/Character.h"
#include "Tree_Top_TerrorCharacter.generated.h"

UCLASS(config=Game)
class ATree_Top_TerrorCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	ATree_Top_TerrorCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
    
    UPROPERTY(EditAnywhere, Category=Rotation)
    float angle = 360;

	void Tick(float Delta) override;
    
    // Damage to the player
    void ProcessDamage(float damage);
    
    // Health of the player
    UFUNCTION(BlueprintCallable, Category = Health)
    float GetHealth() { return Health; }

	void EnableFire();
    
    void DeathCallback();

	void AcquireCoin(int value) { PlayerScore += value; PlayerScoreString = FString((std::to_string(PlayerScore)).c_str()); }

	UAudioComponent* PlayCharacterSound(USoundCue* Sound);

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);
    
    void Rotate(float angle);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	void BeginPlay() override;

	void OnRightClickDown();

	void OnRightClickUp();

	void OnLeftClickDown();

	void OnLeftClickUp();

	void TurnHandler(float Value);

	void OnMouseWheelDown();

	void OnMouseWheelUp();

	void OnToggleCamera();

	void FireGrapple();

	void SetGrapple(AGrapple* grapple) { MyGrapple = grapple; }

	FVector GrappleTrace();

	FTimerHandle mGrappleTimer;

	float mSavedCameraDistance;
	float mDesiredCameraDistance;
	float mDesiredYOffset;
	float mDesiredZOffset;
	float mScrollWheelFactor;

	bool mRightClickDown;
	bool mLeftClickDown;
	bool mCanFire;
	bool mGrappling;

	// True if in the aiming state, determines ability to attack with grapple (and other items?)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = State)
	bool IsAiming;
    
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health)
	float Health = 100;

	UPROPERTY(EditAnywhere, Category = Weapon)
	float GrappleRange;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Score)
	int PlayerScore = 0;
    
    // Visible in the HUD
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Score)
    FString PlayerScoreString = "0";

    bool isDead = false;


protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	// End of APawn interface
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	class USoundCue* FireGrapplesCue;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	class USoundCue* TakeDMGCue;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	class USoundCue* AimCue;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	class USoundCue* StowGrappleCue;

	UPROPERTY(Transient)
	class UAudioComponent* CharacterAC;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UPROPERTY(EditAnywhere, Category = Weapon)
	TSubclassOf<class AGrappleGun> WeaponClass;


	UPROPERTY(EditAnywhere, Category = Weapon)
	TSubclassOf<class AGrapple> GrappleClass;




private:
	class AGrappleGun* MyWeapon;
	class AGrapple* MyGrapple;
};

