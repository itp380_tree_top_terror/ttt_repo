// Fill out your copyright notice in the Description page of Project Settings.

#include "Tree_Top_Terror.h"
#include "Level1ScriptActor.h"
#include "TTTHUD.h"
#include "Tree_Top_TerrorCharacter.h"
#include "Tree_Top_TerrorGameMode.h"

void ALevel1ScriptActor::BeginPlay()
{
    auto controller = GetWorld()->GetFirstPlayerController();
    if (controller) {
        if (HUDClass) {
            HUD = CreateWidget<UTTTHUD>(controller, HUDClass);
            if (HUD) {
                HUD->ShowInstruction(true);
                FString instruction = " W - Forward\n S - Backward\n A - Strafe Left\n D - Strafe Right\n";
                FString complete = instruction + "Space - Jump";
                HUD->SetInstruction(complete);
                HUD->AddToViewport();
            }
        }
    }
    
}

void ALevel1ScriptActor::DisplayCrossHair(bool show)
{
    if (HUD) {
        HUD->DisplayCrossHair(show);
    }
}

void ALevel1ScriptActor::DisplayGameOver()
{
    if (HUD) {
        HUD->DisplayGameOver();
    }
}

void ALevel1ScriptActor::PlayerOutOfBounds(ATree_Top_TerrorCharacter* player)
{
    if (player) {
        // if the player falls out of bounds then decrease health
        player->ProcessDamage(25);
        
        // if the player still lives then respawn
        if (player->GetHealth() > 0) {
            TArray<AActor*> FoundActors;
            UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), FoundActors);
            if (FoundActors.IsValidIndex(0)) {
                auto start = FoundActors[0];
                player->SetActorLocation(start->GetActorLocation());
            }
        } else { // game over
            HUD->DisplayGameOver();
            
            // timer callback
            FTimerHandle handle;
            GetWorld()->GetTimerManager().SetTimer(handle, this, &ALevel1ScriptActor::GameOverTimer, 2, false);
        }
    }
}

void ALevel1ScriptActor::GameOverTimer()
{
    auto gamemode = Cast<ATree_Top_TerrorGameMode>(GetWorld()->GetAuthGameMode());
    gamemode->FinishLevel();
}
