// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Animation/AnimInstance.h"
#include "EnemyAnimInstance.generated.h"

/**
 *  EnemyAnimBP inherits from this class.
 */
UCLASS(Blueprintable, BlueprintType)
class TREE_TOP_TERROR_API UEnemyAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:
    
    // Transition from Idle to Find
    UPROPERTY(BlueprintReadWrite)
    bool FindPlayer = false;

    // Transition from Find to Throw
    UPROPERTY(BlueprintReadWrite)
    bool ThrowWeapon = false;
    
    // Enemy is dead
    UPROPERTY(BlueprintReadWrite)
    bool Dead = false;
	
};
