// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "RangedWeapon.generated.h"

UCLASS()
class TREE_TOP_TERROR_API ARangedWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARangedWeapon();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

    // Throw the weapon
    // Location of the enemy
    virtual void Throw(FVector enemyLocation);
    
    UFUNCTION()
    void DestroyTimer();
    
    UFUNCTION()
    void OnOverlap(class UPrimitiveComponent* Comp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool FromSweep, const FHitResult& SweepResult);
    
private:

    // Where the enemy is
    FVector direction;
    
    // Speed of the weapon
    UPROPERTY(EditDefaultsOnly)
    float speed = 300;
    
    // Damage of the weapon
    UPROPERTY(EditDefaultsOnly)
    float damage = 10;
    
    // time for which the object lives
    const int lifeTime = 7;
    
protected:
    
    // how the ranged weapon looks like
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = StaticMeshComponent)
    UStaticMeshComponent* staticMeshComponent;
};
