// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Enemy.generated.h"

UCLASS()
class TREE_TOP_TERROR_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

    // Enemy health
    float GetHealth() { return Health; }
    
    // Enemy damage
    void ProcessDamage(float damage);
    
    // is the enemy dead
    bool IsEnemyDead() { return Health <= 0;}
    
public:
    
    // Throws the object
    // The object to be thrown should be set below - 'RangedWeaponClass'
    virtual void Throw(FVector playerLocation);
    
    // returns the max range to which the enemy can throw
    float GetMaxRange() { return maxRange; }
        
    // Spawns the weapon
    virtual void SpawnWeapon();
    
protected:
    
    // Set the BP or class to use in the BP Editor
    UPROPERTY(EditAnywhere, Category = Weapon)
    TSubclassOf<class ARangedWeapon> RangedWeaponClass;
    
    // Enemy health
    float Health = 100;

private:
    
    // The weapon we spawned
    class ARangedWeapon* spawnedWeapon;
    
    // the max range to which the enemy can throw
    UPROPERTY(EditDefaultsOnly)
    float maxRange = 100;

	void DieAlready() { Destroy(); }
    
};
