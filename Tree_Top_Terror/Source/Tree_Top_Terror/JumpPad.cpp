// Fill out your copyright notice in the Description page of Project Settings.

#include "Tree_Top_Terror.h"
#include "Tree_Top_TerrorCharacter.h"
#include "JumpPad.h"
#include "Sound/SoundCue.h"


// Sets default values
AJumpPad::AJumpPad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

  BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
  BoxComp->SetWorldScale3D(FVector(200, 200, 10));
  BoxComp->SetupAttachment(RootComponent);
  BoxComp->OnComponentBeginOverlap.AddDynamic(this, &AJumpPad::OnOverlapBegin);
}

// Called when the game starts or when spawned
void AJumpPad::BeginPlay()
{
	Super::BeginPlay();
	
}

void AJumpPad::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    if (OtherActor && (OtherActor != this) && OtherComp)
    {
        auto Player = Cast<ATree_Top_TerrorCharacter>(OtherActor);
        if (Player)
        {
            Launch(Player);
        }
    }
}
void AJumpPad::Launch(ATree_Top_TerrorCharacter* Player)
{
    if (Player->GetMovementComponent()->IsMovingOnGround())//if on top of launchpad
    {
		PlayJumpSound(LaunchCue);
        Player->LaunchCharacter(Player->GetRootComponent()->GetUpVector() * UpSpeed, false, false);
    }
}

UAudioComponent* AJumpPad::PlayJumpSound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
	}
	return AC;
}
