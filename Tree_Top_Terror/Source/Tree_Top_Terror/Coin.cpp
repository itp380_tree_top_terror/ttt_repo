// Fill out your copyright notice in the Description page of Project Settings.

#include "Tree_Top_Terror.h"
#include "Tree_Top_TerrorCharacter.h"
#include "Coin.h"


// Sets default values
ACoin::ACoin()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CoinMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CoinMesh"));
	RootComponent = CoinMesh;

}

// Called when the game starts or when spawned
void ACoin::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACoin::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );


	FRotator ActorRotation = GetActorRotation();

	if (ActorRotation.Yaw < 355)
	{
		ActorRotation.Yaw += 5;
	}
	else
	{
		ActorRotation.Yaw += 5;
	}

	SetActorRotation(ActorRotation);
}


void ACoin::OnActorBeginOverlap(AActor* OtherActor)
{
	if (OtherActor->IsA(ATree_Top_TerrorCharacter::StaticClass()))
	{
		auto character = static_cast<ATree_Top_TerrorCharacter*>(OtherActor);

		character->AcquireCoin(10);
		character->PlayCharacterSound(CoinCue);

		Destroy();
	}
}