// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Level1ScriptActor.h"
#include "Level2ScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class TREE_TOP_TERROR_API ALevel2ScriptActor : public ALevel1ScriptActor
{
	GENERATED_BODY()
	
public:
    
    void BeginPlay() override;
	
	
};
