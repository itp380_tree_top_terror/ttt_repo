// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FloatingBlock.generated.h"

UCLASS()
class TREE_TOP_TERROR_API AFloatingBlock : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFloatingBlock();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
private:
    
  UPROPERTY(VisibleAnywhere)
      FVector Position1;
  UPROPERTY(VisibleAnywhere)
      FVector Position2;
  UPROPERTY(EditAnywhere)
      float BlockVelocity = 15;

  UPROPERTY(EditAnywhere)
	  float MovementOffset;

  bool canMove = true;
  FVector P1_to_P2_normalized;

  void OnOverlapBegin();
  void OnOverlapEnd();
  bool NoIntersections(FVector& first, FVector& second);
};
