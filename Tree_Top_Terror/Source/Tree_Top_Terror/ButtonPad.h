// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "ButtonPad.generated.h"

UCLASS()
class TREE_TOP_TERROR_API AButtonPad : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AButtonPad();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

  //TODO switch pointer type to specific floating block class
  void SetFloatingBlock(AActor* block) { mFloatingBlock = block; };

  UPROPERTY(VisibleAnywhere)
      class UBoxComponent* BoxComp;

  UPROPERTY(VisibleAnywhere)
      bool mButtonSellected = false;

  UPROPERTY(EditAnywhere)
      //TODO switch pointer type to specific floating block class
      AActor* mFloatingBlock = nullptr;
  UPROPERTY(EditAnywhere)
      float mBlockVelociy;
  UFUNCTION()
      void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

  UFUNCTION()
      void MoveBlock();
	
};
