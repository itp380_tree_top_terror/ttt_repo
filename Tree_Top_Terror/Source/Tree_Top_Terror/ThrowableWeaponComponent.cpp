// Fill out your copyright notice in the Description page of Project Settings.

#include "Tree_Top_Terror.h"
#include "ThrowableWeaponComponent.h"


// Sets default values for this component's properties
UThrowableWeaponComponent::UThrowableWeaponComponent()
{
	bWantsBeginPlay = false;
	PrimaryComponentTick.bCanEverTick = true;
}

void UThrowableWeaponComponent::Throw(FVector direction, float distance)
{
    // TODO: Throws the weapon to the distance using the direction vector

}

// Called every frame
void UThrowableWeaponComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}

