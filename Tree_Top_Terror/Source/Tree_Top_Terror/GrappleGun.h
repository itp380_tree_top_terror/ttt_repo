// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Grapple.h"
#include "GameFramework/Actor.h"
#include "GrappleGun.generated.h"

UCLASS()
class TREE_TOP_TERROR_API AGrappleGun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGrappleGun();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void SetMyOwner(class ATree_Top_TerrorCharacter* owner);

	void AttachGrapple(class AGrapple* grapple);

//protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	USkeletalMeshComponent* WeaponMesh;

private:
	class ATree_Top_TerrorCharacter* MyOwner;
	class AGrapple* MyGrapple;


};
