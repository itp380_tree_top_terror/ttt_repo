// Fill out your copyright notice in the Description page of Project Settings.

#include "Tree_Top_Terror.h"
#include "TTTHUD.h"

void UTTTHUD::SetInstruction(FString text)
{
    this->instruction = text;
}

void UTTTHUD::ShowInstruction(bool showInstruction)
{
    this->show = showInstruction ? ESlateVisibility::Visible : ESlateVisibility::Hidden;
}

void UTTTHUD::DisplayCrossHair(bool showCrossHair)
{
    this->displayCrossHair = showCrossHair ? ESlateVisibility::Visible : ESlateVisibility::Hidden;
}

void UTTTHUD::DisplayGameOver()
{
    // display game over
    this->displayGameOver = ESlateVisibility::Visible;
}
