// Fill out your copyright notice in the Description page of Project Settings.

#include "Tree_Top_Terror.h"
#include "FloatingBlock.h"


// Sets default values
AFloatingBlock::AFloatingBlock()
	: MovementOffset(5)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}
void AFloatingBlock::OnOverlapBegin()
{
    //Verify actor isn't on top of block (should move while actor standing on it but
    //not when actor is blocking path)
    canMove = false;
}
void AFloatingBlock::OnOverlapEnd()
{
    canMove = true;
}
// Called when the game starts or when spawned
void AFloatingBlock::BeginPlay()
{
	Super::BeginPlay();
  Position1 = GetActorLocation();

  bool foundSuitableSecondPos = false;
  while (!foundSuitableSecondPos)
  {
      float secondPosDirection = rand()%360;
      Position2 = Position1 + FVector(MovementOffset, 0, 0).RotateAngleAxis(secondPosDirection, FVector(0, 0, 1)) * 300;
      if (NoIntersections(Position1, Position2))
      {
          foundSuitableSecondPos = true;
      }
  }
  P1_to_P2_normalized = Position2 - Position1;
  P1_to_P2_normalized.Normalize();
}

bool AFloatingBlock::NoIntersections(FVector& first, FVector& second)
{
    //check there aren't intersections between the first and second points (clear path)
    FHitResult Hit(ForceInit); 
    FCollisionQueryParams TraceParams(FName(TEXT("PathTrace")), true, Instigator);
    TraceParams.bTraceAsyncScene = true; 
    TraceParams.bReturnPhysicalMaterial = true;
    bool isCollission = GetWorld()->LineTraceSingleByObjectType(Hit, first, second, FCollisionObjectQueryParams::AllObjects, TraceParams);
    return !isCollission; 
}

// Called every frame
void AFloatingBlock::Tick( float DeltaTime )
{
	Super::Tick(DeltaTime);
  //go to second position
  FVector currPos = GetActorLocation();
  float distSqrd = currPos.DistSquared(currPos, Position2);
  if (distSqrd >= 10*10)
  {
      if (NoIntersections(currPos,Position2))
      {
          //advance by P1_to_P2_normalized * BlockVelocity/deltaTime;
          AddActorLocalOffset(P1_to_P2_normalized*BlockVelocity * DeltaTime, true);
      }
  }
  else //if reached second pos, swap Pos1 & Pos2 and repeat
  {
      P1_to_P2_normalized = -P1_to_P2_normalized;
      FVector temp = Position2;
      Position2 = Position1;
      Position1 = temp;
  }
}

