// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class TREE_TOP_TERROR_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
	
public:
    // Update every frame
    void Tick(float) override;
    
    // begin player
    void BeginPlay() override;

    // Called from the AEnemy Anim BP to notify that the throw animation has ended
    // Then we transition to the idle state
    UFUNCTION(BlueprintCallable, Category = "State")
    void ManageTransitionFromThrowToIdle();
    
    UFUNCTION(BlueprintCallable, Category = "Weapon")
    void ThrowWeapon();
    
    // Called by the enemy when he dies
    void EnemyDead();
    
private:
    
    // Returns true if the player is within range of the enemy
    bool PlayerWithInRange(FVector);
    
    // Rotates the enemy to face the player
    void RotateEnemy(FVector);
    
    // Returns the distance from player
    float DistanceFromPlayer(FVector);
    
    // Callback when we stay in the idle state for 'IdleStateTime'
    void IdleStateTimerCallback();
        
private:
    // State Machine
    enum State {
        Idle,
        Find,
        Throw,
        Dying,
        Dead,
    };
	
    // initial state
    State currentState = Idle;

    // 2 secs in the idle state
    float IdleStateTime = 2;
    
    // The handle for the timer in the idle state
    FTimerHandle idleStateTimerHandle;
    
    // the main player
    ACharacter* Player;
    
    // The animation instance associated with the enemy mesh
    // We will use this to manage the animations associated with the enemy
    class UEnemyAnimInstance* AnimInstance;
};
