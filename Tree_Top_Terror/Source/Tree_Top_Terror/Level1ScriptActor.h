// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/LevelScriptActor.h"
#include "Level1ScriptActor.generated.h"

/**
 *  Level 1 of the game
 */
UCLASS()
class TREE_TOP_TERROR_API ALevel1ScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()
	
public:
    
    // Initialization
    void BeginPlay() override;
    
    // display the crosshair
    void DisplayCrossHair(bool);
    
    void DisplayGameOver();
    
    // Called by the trigger box when the player is out of bounds
    UFUNCTION(BlueprintCallable, Category = PlayerManagement)
    void PlayerOutOfBounds(class ATree_Top_TerrorCharacter *player);
    
protected:
    
    // Which HUD BP to use
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = UI)
    TSubclassOf<class UTTTHUD> HUDClass;
    
    void GameOverTimer();
    
    // the HUD
    class UTTTHUD *HUD;
};
